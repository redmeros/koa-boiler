import * as Koa from 'koa';
import * as bodyParser from 'koa-bodyparser';
import * as logger from 'koa-logger';
import * as jwt from 'koa-jwt';
import * as cors from 'koa-cors';
import * as helmet from 'koa-helmet';

import * as HttpStatus from 'http-status-codes';

import response_time from './response-time/response-time';


import movieController from '../movie/movie.controller';
import userController from '../user/user.controller';
import { UserPrivateController } from '../user/user-private.controller';


console.log("importing koa");
const app:Koa = new Koa();

app.use(response_time());
app.use(cors());
app.use(helmet());
app.use(logger());
app.use(bodyParser());
app.use(async (ctx: Koa.Context, next: () => Promise<any>) => {
  try {
    await next();
  } catch (error) {
    ctx.status = error.statusCode || error.status || HttpStatus.INTERNAL_SERVER_ERROR;
    error.status = ctx.status;
    ctx.body = { error };
    ctx.app.emit('error', error, ctx);
  }
});

app.use(userController.routes());
app.use(userController.allowedMethods());

app.use(jwt({
  secret: process.env.SECRET || "JWT_SECRET"
}));

const b = new UserPrivateController(app);

app.on('error', console.error); 

export default app;
