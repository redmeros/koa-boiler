import * as Koa from 'koa';

function response_time() : Koa.Middleware {
  return async (ctx: Koa.Context, next: () => Promise<any>) => {
    const start = Date.now();
    await next();
    const delta = Math.ceil(Date.now() - start);
    ctx.set('X-Response-Time', `${delta}ms`);
  };
}

export default response_time;