import * as Koa from 'koa';
import * as Router from 'koa-router';
import { Repository, getRepository, Entity } from 'typeorm';
import db from '../../database/database.connection';
export type ObjectType<T> = { new () : T} | Function;


export class BaseController<T> {
    protected keyword: string;

    protected routerOpts: Router.IRouterOptions;
    protected router: Router;
    protected repo: Repository<T>;
    protected myType: ObjectType<T>;

    constructor(keyword: string, type: ObjectType<T>, routerOpts?: Router.IRouterOptions) {
        this.keyword = keyword;
        this.myType = type;
        if (!routerOpts) {
            this.routerOpts = {
                prefix: `/${this.keyword}`
            }
        } else {
            this.routerOpts = routerOpts;
        }

        this.router = new Router(this.routerOpts);
    }
    
    async getRepo(): Promise<Repository<T> > {
        const dbconn = await db;
        return dbconn.getRepository(this.myType);
    };

    //lista
    async configureGet() {
        this.router.get('/', async (ctx: Koa.Context, next: () => Promise<any>) => {
            const repo = await this.getRepo();
            const lst = await repo.find();
            ctx.body = lst;
        });
    }

    //update
    // async configurePatch() {
    //     this.router.patch('/', async (ctx: Koa.Context, next: () => Promise<any>) => {
    //         const repo = await this.getRepo();
    //         const dbent = await repo.findOne(ctx.state.user.data.id);
    //         dbent
    //     });
    // }

    getRouter(): Router {
        return this.router;
    }

    registerRouter(app: Koa) {
        app.use(this.router.routes());
        app.use(this.router.allowedMethods());
    }
}