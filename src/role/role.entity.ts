import { PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, Entity, OneToMany } from "typeorm";
import User from "../user/user.entity";


@Entity()
export default class Role {
    @PrimaryGeneratedColumn()
    id: string;

    @Column( { nullable: false, default: 0 })
    level: number;

    @CreateDateColumn()
    created: string;

    @UpdateDateColumn()
    updated: string;

    @Column( {unique: true})
    name: string;

    @OneToMany(type => User, user => user.role)
    users: User[];
}