import * as Koa from 'koa';
import * as Router from 'koa-router';
import * as HttpStatus from 'http-status-codes';
import { Repository, getRepository } from 'typeorm';
import movieEntity from './movie.entity';
import User from '../user/user.entity';

const routerOpts: Router.IRouterOptions = {
  prefix: '/movies',
};

const router: Router = new Router(routerOpts);

router.get('/', async (ctx : Koa.Context) => {
  const movieRepo:Repository<movieEntity> = getRepository(movieEntity);
  const movies = await movieRepo.find();
  ctx.body = {
    data: {
      movies,
    },
  };
});

router.get('/:movie_id', async (ctx:Koa.Context) => {
  const movieRepo:Repository<movieEntity> = getRepository(movieEntity);
  const movie = await movieRepo.findOne(ctx.params.movie_id);

  if (!movie) {
    ctx.throw(HttpStatus.NOT_FOUND);
  }

  ctx.body = {
    movie,
  };
});

router.post('/', async (ctx:Koa.Context) => {

  const user: User = ctx.state.user;
  
  if (!user.role || user.role.level < 500) {
    ctx.throw("Role not set for user", HttpStatus.FORBIDDEN);
  }

  const movieRepo:Repository<movieEntity> = getRepository(movieEntity);
  const movie: movieEntity = movieRepo.create(ctx.request.body as movieEntity);
  await movieRepo.save(movie);

  ctx.body = {
    data: { movie },
  };
});

router.delete('/:movie_id', async (ctx:Koa.Context) => {
  const movieRepo : Repository<movieEntity> = getRepository(movieEntity);
  const movie = await movieRepo.findOne(ctx.params.movie_id);
  if (!movie) {
    ctx.throw(HttpStatus.NOT_FOUND);
  }
  await movieRepo.delete(movie);
  ctx.status = HttpStatus.NO_CONTENT;
});

router.put('/:movie_id', async (ctx:Koa.Context) => {
  const movieRepo : Repository<movieEntity> = getRepository(movieEntity);
  const movie: movieEntity = await movieRepo.findOne(ctx.params.movie_id);
  if (!movie) {
    ctx.throw(HttpStatus.NOT_FOUND);
  }

  const updatedMovie = await movieRepo.merge(movie, ctx.request.body);
  movieRepo.save(updatedMovie);

  ctx.body = {
    data: { movie: updatedMovie },
  };
});

export default router;
