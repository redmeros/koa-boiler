import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn, 
    ManyToOne} from 'typeorm';

import { IsEmail } from 'class-validator';
import Role from '../role/role.entity';

@Entity()
export default class User {
  @PrimaryGeneratedColumn()
    id: string;

  @Column({ unique: true })
    username: string;

  @IsEmail()
  @Column({ unique: true, nullable: false })
  email: string;

  @Column({ length: 60 })
  password: string;

  @CreateDateColumn()
  created: string;

  @UpdateDateColumn()
  updated: string;

  @ManyToOne(type => Role, role => role.users)
  role: Role;
}

