import * as Koa from 'koa';
import * as bcrypt from 'bcrypt';
import * as HttpStatus from 'http-status-codes';

import { BaseController } from '../app/base/base-controller';
import User from '../user/user.entity';


export class UserPrivateController extends BaseController<User> {
    constructor(app: Koa) {
        super('user', User);
        this.configureGet();
        this.configurePatch();
        this.configureChangePass();
        
        this.registerRouter(app);
    }

    removeSensitiveData(ent: User) {
        delete ent.password;
        delete ent.created;
        delete ent.updated;
        return ent;
    }

    async configureGet() {
        this.router.get('/', async (ctx: Koa.Context, next: () => Promise<any>) => {
            const repo = await this.getRepo();
            const currentUser: User = ctx.state.user.data;
            const dbUser = await repo.findOne(currentUser.id);
            ctx.body = this.removeSensitiveData(dbUser);
        });
    }

    async configurePatch() {
        this.router.patch('/', async (ctx: Koa.Context, next: () => Promise<any>) => {
            const repo = await this.getRepo();
            const currentUser: User = ctx.state.user.data;
            const dbUser = await repo.findOne(currentUser.id);
            dbUser.email = ctx.request.body.email;
            const nUser = await repo.save(dbUser);
            ctx.body = this.removeSensitiveData(nUser);
        });
    }

    async configureChangePass() {
        this.router.patch('/changepassword', async (ctx: Koa.Context) => {
            if (!ctx.request.body.password) {
                ctx.throw(HttpStatus.BAD_REQUEST);
            }
            const repo = await this.getRepo();
            const currentUser: User = ctx.state.user.data;
            let dbUser = await repo.findOne(currentUser.id);
            const hash = await bcrypt.hash(ctx.request.body.password, 12);
            dbUser.password = hash;
            dbUser = await repo.save(dbUser);
            ctx.body = this.removeSensitiveData(dbUser);
        });
    }
}