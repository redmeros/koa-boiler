import * as Koa from 'koa';
import * as Router from 'koa-router';
import * as HttpStatus from 'http-status-codes';

import { getRepository } from 'typeorm';
import { getConnectionManager } from 'typeorm';

import * as bcrypt from 'bcrypt';
import * as jsonwebtoken from 'jsonwebtoken';

import userEntity from './user.entity';
import roleEntity from '../role/role.entity';

const SECRET = process.env.SECRET || "JWT_SECRET";

const routerOpts: Router.IRouterOptions = {
    prefix: '/users',
}

const router: Router = new Router(routerOpts);

router.post('/install', async(ctx: Koa.Context, next: () => Promise<any>) => {
    const userRepo = getRepository(userEntity);
    const roleRepo = getRepository(roleEntity);
    const userCount = await userRepo.count();
    const roleCount = await userRepo.count();

    if (userCount !== 0 || roleCount !== 0) {
        ctx.throw("Not allowed", HttpStatus.GONE);
    }

    console.log('test');
});

router.post('/login', async(ctx: Koa.Context, next: () => Promise<any>) => {
    const b = ctx.request.body;
    if (!b.username || !b.password) {
        ctx.throw(HttpStatus.BAD_REQUEST);
    }
    
    const userRepo = getRepository(userEntity);

    const user = await userRepo.findOne({
        username: ctx.request.body.username
    });

    if (!user) {
        ctx.throw(
            'User not found.',
            HttpStatus.NOT_FOUND
        );
    }

    const {
        password,
        ...userInfoWithoutPassword
    } = user;

    if (await bcrypt.compare(ctx.request.body.password, user.password)) {
        ctx.body = {
            token: jsonwebtoken.sign({ data: userInfoWithoutPassword, exp: Math.floor(Date.now() / 1000) + (60 * 60) }, SECRET),
        }
        return;
    } else {
        ctx.throw("Wrong username or password" ,HttpStatus.UNAUTHORIZED);
    }
    return;
});

router.post('/sign-up', async(ctx: Koa.Context) => {
    const userRepo = getRepository(userEntity);
    const body = ctx.request.body;
    if (!body.username || !body.email || !body.password) {
        ctx.throw("Invalid data", HttpStatus.BAD_REQUEST);
    }

    let existingUser = await userRepo.findOne(
        {username: ctx.request.body.username}
        );
    if (existingUser) {
        ctx.throw('User with given name already exists', HttpStatus.CONFLICT);
    }

    existingUser = await userRepo.findOne(
        {
            email: ctx.request.body.email
        }
    );
    if (existingUser) {
        ctx.throw('User with given email already exists', HttpStatus.CONFLICT);
    }

    const pass: string = ctx.request.body.password;
    const hash = await bcrypt.hash(pass, 12);

    const user = userRepo.create({
        username: ctx.request.body.username,
        password: hash,
        email: ctx.request.body.email
    });
    
    await userRepo.save(user);
    delete user.password;
    delete user.created;
    delete user.updated;

    ctx.body = user;
});

export default router;
